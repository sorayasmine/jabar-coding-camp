// Soal 1
var nilai = 77;

if(nilai >= 85){
  console.log('Indeks Nilai anda A ');
} else if (nilai >=75 && nilai < 85){
  console.log('Indeks Nilai anda B');
} else if (nilai >=65 && nilai < 75){
  console.log('Indeks Nilai anda C');
} else if (nilai >= 55 && nilai < 65){
  console.log('Indeks Nilai anda D');
} else {
  console.log('Indeks Nilai anda E');
}
// Soal 2
var tanggal = 4;
var bulan = 'Februari';
var tahun = 1997;

switch (bulan) {
  case 'Januari' : {
    console.log('Saya tidak lahir pada bulan ini');
    break;
  }
  case 'Februari' : {
    console.log(tanggal + ' '+ bulan + ' ' + tahun);
    break;
  }
  case 'Maret' : {
    console.log('Ini bukan bulan lahir saya');
    break;
  }
  case 'April' : {
    console.log('Saya tidak lahir pada bulan ini');
    break;
  }
  case 'Mei' : {
    console.log(tanggal + ' '+ bulan + ' ' + tahun);
    break;
  }
  case 'Juni' : {
    console.log('Ini bukan bulan lahir saya');
    break;
  }
  case 'Juli' : {
    console.log('Saya tidak lahir pada bulan ini');
    break;
  }
  case 'Agustus' : {
    console.log(tanggal + ' '+ bulan + ' ' + tahun);
    break;
  }
  case 'September' : {
    console.log('Ini bukan bulan lahir saya');
    break;
  }
  case 'Oktober' : {
    console.log('Saya tidak lahir pada bulan ini');
    break;
  }
  case 'November' : {
    console.log(tanggal + ' '+ bulan + ' ' + tahun);
    break;
  }
  case 'Desember' : {
    console.log('Ini bukan bulan lahir saya');
    break;
  }
  default : {
    console.log('Pastikan bulan yang dimasukkan benar');
    } 
}

// Soal 3
var s = '';
for (var i = 0; i < 7; i++){
  for (var j = 0; j <= i; j++){
      s += '#';
  }
  s += '\n'
}
console.log(s);

// Soal 4
var str = '';

for(var i = 1; i<= 10; i++){
  str += '=';
  if(i%3 == 2){
    console.log(i +" - I love programming");
  } else if (i%3 == 1){
    console.log(i +" - I love Javascript");
  } else if (i%3 == 0){
    console.log(i +" - I love VueJS");
    console.log(str);
  }
}