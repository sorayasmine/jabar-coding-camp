// Soal 1
let luasPersegiPanjang =(a, b)=>{
    return a*b;
  }
  console.log(luasPersegiPanjang(1,2));
  
  let kelilingPersegi = (p, l) =>{
      return 2*(p+l);
  }
  console.log(kelilingPersegi(1,2));
  
  // Soal 2
  const newFunction = (firstName, lastName)=>{
      return {
        firstName,
        lastName,
        fullName: function(){
          console.log(`${firstName} ${lastName}`);
        }
      }
    }
     
  //Driver Code 
  newFunction("William", "Imoh").fullName();
  
  // Soal 3
  const newObject = {
  firstName: "Tiara",
  lastName: "Yasmine Soraya",
  address: "Jalan GKGB",
  hobby: "reading manhwas",
  }
  const {firstName, lastName, address, hobby} = newObject;
  console.log(firstName, lastName, address, hobby);
  
  // Soal 4
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  const combined = [...west, ...east];
  //Driver Code
  console.log(combined);
  
  // Soal 5
  const planet = "earth"; 
  const view = "glass" ;
  var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`; 
  console.log(before);