var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// Promise.all([readBooksPromise(10000, books[0]),readBooksPromise(7000, books[1]),readBooksPromise(5000, books[2]), readBooksPromise(1000, books[3])]).then((result) =>{
//     console.log(result);
// });

readBooksPromise(10000, books[0])
.then(result => console.log(result));
readBooksPromise(7000, books[1])
.then(result => console.log(result));
readBooksPromise(5000, books[2])
.then(result => console.log(result));
readBooksPromise(1000, books[3])
.then(result => console.log(result));   

// readBooksPromise(10000, books[0])
//     .then(result => console.log(result));